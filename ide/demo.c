#include <stdio.h>

/*tec_factorial:
	returns the factorial of n
	n should not be greater than 170 to avoid overflow
	returns -1 in case of error (invalid n or overflow)
*/
double		tec_factorial(int n);

double tec_factorial(int n){

	if(n < 0 || n > 170)
		return -1;
	if(n == 0 || n == 1)
		return 1;

	double result = 1;
	while(n){
		result *= (double) n;
		n -= 1;
	}
	return result;

}//tec_factorial*/

int main(){

	int i = 0;
	double f = 0;
	while(i < 20){
		f = tec_factorial(i);
		printf("Factorial of %d is %g\n", i, f);
		i += 1;
	}

	return 0;

}//main*/
