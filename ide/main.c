#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>

#include "tc.h"

// this relates to tec_process
// stderr should use same size
#define TEC_STDOUT_BUFFER_SIZE 1024

// most significant digit in 1 byte char
#define TEC_MOST_SIG_BIT 128
#define TEC_2ND_MOST_SIG_BIT 64

typedef struct _tec_process tec_process;

struct _tec_process{

	char buf_stdout[TEC_STDOUT_BUFFER_SIZE];
	char buf_stderr[TEC_STDOUT_BUFFER_SIZE];

	//stdout callback for process
	void (*tp_output_cb)(void *tp, void *dp);
	void *dp;	//data_pointer, argument 
	//stderr callback for process
	void (*tp_output_error_cb)(void *tp, void *dp);
	void *dp_error;

#define PARENT_READ_PIPE 0
#define PARENT_WRITE_PIPE 1
#define PARENT_READ_ERROR_PIPE 2
#define READ_FD  0
#define WRITE_FD 1

	pid_t child;

	int pipes[3][2];

};

/*tec_string_to_int:
	converts a string to an integer
	string may not contain anything in front of number except '-' or '+'
	does not safeguard against overflow
*/
int			tec_string_to_int(char *s);

/*tec_string_copy:
	safer alternative to strcpy or even strncpy
	int size is the size of the destination
	these functions guarantee that at most size - 1 bytes will be written to destination plus a NULL character
	this prevents buffer overflows
	this guarantees you get a NULL terminated string in destination (unlike strncpy)
	this function will not cut off the copying in the middle of a UTF8 codepoint when out of space
	returns 1 if all was copied right
	returns 2 if source could not be fully copied
	returns 0 in case of error

	these functions assume char *source is a correctly formatted UTF8 string
*/
int			tec_string_copy(char *destination, char *source, int size);

/*tec_string_split:

	splits a string where char c does occur
	num will give you the number of times char c did occur in the string
	num is also index of last string in returned char**
	CAUTION: there is one more string than occurences of char c, this might include empty strings

	the string str will be modified so make sure to make a copy if needed

	note that some character pointers may be NULL pointers
	if 2 or more characters c are right next to each other

	free the returned char ** when done

	returns NULL in case of error
*/
char**		tec_string_split(char *str, char c, int *num);

/*tec_file_get_size:
	returns the size of the file in bytes
	returns -1 in case of error e.g. file does not exist
*/
int64_t		tec_file_get_size(char *path);

/*tec_sys_launch_process:
	launches a process with redirected input and output
	char *cmd: the command to be executed
	command should not be longer than 1024 characters (including terminating NULL)
	void (*func_out)(): callback for the output (stdout)
	void (*func_error)(): callback for the output (stderr)

	func_out and func_error should have prototypes:
	void func_out(tec_process *tp, void *data);
	void func_error(tec_process *tp, void *data);

tec_process_input(tec_process *p);
	pass on input to the process launched with tec_sys_launch_process

	free the struct returned by tec_sys_launch_process once the process is no longer running
*/
tec_process*	tec_sys_launch_process(char *cmd, void (*func_out)(), void *data_out, void (*func_error)(), void *data_error);
void			tec_sys_process_input(tec_process *tp, char *text);


void ide_process_gdb_errors(tec_process *tp, void *data);
void ide_process_gdb_output(tec_process *tp, void *data);
char* ide_file_get_contents(char *path);
void ide_display_contents(char *buffer, int line);

int tec_string_to_int(char *s){

	if(!s)
		return 0;

	int sign = 1;
	int result = 0;

//	while(tec_char_is_white_space(*s)){
//		s += 1;
//	}
	if(*s == '-'){
		sign = -1;
		s += 1;
	}else{
		if(*s == '+'){
			s += 1;
		}
	}

	while(*s){
		if(*s > '9' || *s < '0'){
			return result * sign;
		}
		result *= 10;
		result += *s - '0';
		s += 1;
	}

	return result * sign;

}//tec_string_to_int*/

int tec_string_copy(char *destination, char *source, int size){

	if(!destination)
		return 0;
	if(!source){
		*destination = 0;
		return 1;
	}
	if(size <= 0)
		return 0;

	size -= 1;

	int i = 0;
	while(*source && i < size){
		destination[i] = *source;
		source += 1;
		i += 1;
	}

	// we don't want to cut off the copying in the middle of a UTF8 codepoint
	// firstly check whether the next byte of source is either not present or the start of a codepoint
	if(*source && (*source & TEC_MOST_SIG_BIT) && !(*source & TEC_2ND_MOST_SIG_BIT) ){
		i -= 1;
		// this while loop goes back while we have the 2nd, 3rd or 4th byte of a UTF8 codepoint
		while( (destination[i] & TEC_MOST_SIG_BIT) && !(destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			i -= 1;
		}
		// this goes back from the head of a UTF8 codepoint
		if( (destination[i] & TEC_MOST_SIG_BIT) && (destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			destination[i] = 0;
		}else{
			// should never happen, this would be invalid UTF8
			destination[i] = 0;
			return 0;
		}
	}

	destination[i] = '\0';

	if(*source){
		return 2;
	}else{
		return 1;
	}

}//tec_string_copy*/

char** tec_string_split(char *str, char c, int *num){

	if(!str)
		return NULL;
	if(!c)
		return NULL;

	// we start by assuming that there will be no more than 32 instances of c
	int original_limit = 32;
	int limit = original_limit;
	char **table[26];
	int table_index = 0;
	int tmp_num = 0;
	*num = 0;

	char **res = (char **) malloc(sizeof(char *) * limit);
	if(!res){
		return NULL;
	}
	table[table_index] = res;

	res[0] = str;
	while(*str){
		if(*str == c){
			tmp_num += 1;
			*num += 1;
			if(tmp_num == limit){
				limit *= 2;
				table_index += 1;
				res = (char **) malloc(sizeof(char *) * limit);
				if(!res){
					return NULL;
				}
				table[table_index] = res;
				tmp_num = 0;
			}
			*str = '\0';
			str += 1;
			if(*str){
				res[tmp_num] = str;
			}else{
				//Note(GI) this deals with case where char c is last character in the string
				res[tmp_num] = NULL;
			}
		}else{
			str += 1;
		}
	}

	if(*num < original_limit){
		res[(*num)+1] = NULL;
		return res;
	}

	char **real_res = (char **) malloc(sizeof(char *) * (*num + 2));
	int ti = 0;
	int n = 0;
	int n2 = 0;
	limit = original_limit;
	while(ti <= table_index){

		res = table[ti];
		n2 = 0;
		while(n2 < limit && n <= *num){
			real_res[n] = res[n2];
			n2 += 1;
			n += 1;
		}

		free(res);
		limit *= 2;
		ti += 1;

	}
	real_res[(*num) + 1] = NULL;

	return real_res;

}//tec_string_split*/

int64_t tec_file_get_size(char *path){

	if(!path)
		return -1;

	struct stat st;
	int error = stat(path, &st);
	if(!error){
		return (int64_t) st.st_size;
	}else{
		return -1;
	}

	return -1;

}//tec_file_get_size*/

void* t_sys_launch_process_output(void *data){

	if(!data)
		return 0;

	tec_process *tp = (tec_process *) data;

	void (*tp_output_cb)(tec_process *tpr, void *dp);
	tp_output_cb = (void *) tp->tp_output_cb;

	if(!tp_output_cb){
		return 0;
	}

	int count = 0;
	count = read(tp->pipes[PARENT_READ_PIPE][READ_FD], tp->buf_stdout, TEC_STDOUT_BUFFER_SIZE-1);

	while(count > 0) {
		tp->buf_stdout[count] = 0;
		tp_output_cb(tp, tp->dp);
		count = read(tp->pipes[PARENT_READ_PIPE][READ_FD], tp->buf_stdout, TEC_STDOUT_BUFFER_SIZE-1);
	}

	return 0;

}//t_sys_launch_process_output*/

void* t_sys_launch_process_error(void *data){

	if(!data)
		return 0;

	tec_process *tp = (tec_process *) data;

	void (*tp_output_error_cb)(tec_process *tpr, void *dp);
	tp_output_error_cb = (void *) tp->tp_output_error_cb;

	if(!tp_output_error_cb)
		return 0;

	int count = 0;
	count = read(tp->pipes[PARENT_READ_ERROR_PIPE][READ_FD], tp->buf_stderr, TEC_STDOUT_BUFFER_SIZE-1);

	while(count > 0) {
		tp->buf_stderr[count] = 0;
		(*tp_output_error_cb)(tp, tp->dp_error);
		count = read(tp->pipes[PARENT_READ_ERROR_PIPE][READ_FD], tp->buf_stderr, TEC_STDOUT_BUFFER_SIZE-1);
	}

	return 0;

}//t_sys_launch_process_error*/

tec_process* tec_sys_launch_process(char *cmd, void (*func_out)(), void *data_out, void (*func_error)(), void *data_error){

	if(!cmd)
		return NULL;

	int len = strlen(cmd);
	if(len > 1023)
		return 0;

	tec_process *tp = (tec_process *) malloc(sizeof(tec_process));
	tp->buf_stdout[0] = 0;
	tp->buf_stderr[0] = 0;
	tp->tp_output_cb = func_out;
	tp->dp = data_out;
	tp->tp_output_error_cb = func_error;
	tp->dp_error = data_error;

	char path[1024];
	tec_string_copy(path, cmd, 1024);
	int num = 0;
	char **spl = tec_string_split(path, ' ', &num);

	if(pipe(tp->pipes[PARENT_READ_PIPE]) == -1){
		printf("tec_sys_launch_process error: failed to create pipe.");
	}
	if(pipe(tp->pipes[PARENT_WRITE_PIPE]) == -1){
		printf("tec_sys_launch_process error: failed to create pipe.");
	}
	if(pipe(tp->pipes[PARENT_READ_ERROR_PIPE]) == -1){
		printf("tec_sys_launch_process error: failed to create pipe.");
	}

	pid_t pid = fork();

	if(pid){

		//parent side
		tp->child = pid;
		// close fds not required by parent
		close(tp->pipes[PARENT_WRITE_PIPE][READ_FD]);
		close(tp->pipes[PARENT_READ_PIPE][WRITE_FD]);
		close(tp->pipes[PARENT_READ_ERROR_PIPE][WRITE_FD]);

	}else{

		//child side
		dup2(tp->pipes[PARENT_WRITE_PIPE][READ_FD], STDIN_FILENO);
		dup2(tp->pipes[PARENT_READ_PIPE][WRITE_FD], STDOUT_FILENO);
		dup2(tp->pipes[PARENT_READ_ERROR_PIPE][WRITE_FD], STDERR_FILENO);

		close(tp->pipes[PARENT_WRITE_PIPE][READ_FD]);
		close(tp->pipes[PARENT_READ_PIPE][WRITE_FD]);
		close(tp->pipes[PARENT_READ_PIPE][READ_FD]);
		close(tp->pipes[PARENT_WRITE_PIPE][WRITE_FD]);
		close(tp->pipes[PARENT_READ_ERROR_PIPE][WRITE_FD]);
		close(tp->pipes[PARENT_READ_ERROR_PIPE][READ_FD]);

		if(execv(spl[0], spl) == -1){
			printf("tec_sys_launch_process: some error occured (execv)");
		}

	}

	pthread_t thr_output;
	pthread_t thr_error;

	pthread_create(&thr_output, NULL, &t_sys_launch_process_output, (void *) tp);
	pthread_create(&thr_error, NULL, &t_sys_launch_process_error, (void *) tp);

	return tp;

}//tec_sys_launch_process*/

void tec_sys_process_input(tec_process *tp, char *text){

	if(!tp)
		return;
	if(!text)
		return;

	write(tp->pipes[PARENT_WRITE_PIPE][WRITE_FD], text, strlen(text));

}//tec_sys_process_input*/

void ide_process_gdb_errors(tec_process *tp, void *data){

	if(!tp->buf_stderr[0]){
		return;
	}

	printf("%s%s%s\n", TC_RED, tp->buf_stderr, TC_NRM);

}//ide_process_gdb_errors*/

void ide_process_gdb_output(tec_process *tp, void *data){

	if(!tp->buf_stdout[0])
		return;

	char *out = tp->buf_stdout;
	char *buffer = (char *) data;

	int nr = 0;
	while(*out && !nr){
		if(*out >= '0' && *out <= '9'){
			nr = tec_string_to_int(out);
		}
		while(*out && *out != 10){
			out += 1;
		}
		if(*out){
			out += 1;
		}
	}

	if(nr){
		ide_display_contents(buffer, nr);
	}

	if(tp->buf_stdout[0]){
		printf("%s%s%s\n", TC_YEL, tp->buf_stdout, TC_NRM);
	}

}//ide_process_gdb_output*/

char* ide_file_get_contents(char *path){

	if(!path)
		return NULL;

	int64_t size = tec_file_get_size(path);
	if(size == -1)
		return NULL;

	size += 1;
	FILE *fp = fopen(path, "r");
	if(!fp){
		return NULL;
	}
	char *buffer = (char *) malloc(sizeof(char) * size);
	fread(buffer, sizeof(char), size, fp);
	fclose(fp);
	buffer[size] = 0;
	return buffer;

}//ide_file_get_contents*/

void ide_display_contents(char *buffer, int line){

	tc_clear_screen();
	tc_move_cursor(0, 0);

	int l = 1;

	while(*buffer){
		if(l == line){
			printf("%s", TC_RED);
		}else{
			printf("%s", TC_NRM);
		}

		while(*buffer && *buffer != 10){
			putchar(*buffer);
			buffer += 1;
		}
		if(*buffer){
			l += 1;
			putchar(*buffer);
			buffer += 1;
		}
	}
	tc_move_cursor(0, 39);

}//ide_display_contents*/

int main(int argc, char **argv){

	char *buffer = ide_file_get_contents("demo.c");
	tc_enter_alt_screen();

	ide_display_contents(buffer, 0);

	tec_process *tp = tec_sys_launch_process("/usr/bin/gdb demo", (void *) &ide_process_gdb_output, (void *) buffer, (void *) &ide_process_gdb_errors, 0);

	tec_sys_process_input(tp, "b 26\nr\n");

	char input[30];
	int n = read(0, input, 29);
	input[n] = 0;
	while(n && strcmp("exit\n", input) ){
		tec_sys_process_input(tp, input);
		n = read(0, input, 29);
		input[n] = 0;
	}

	tc_exit_alt_screen();

	return 0;

}//main*/
