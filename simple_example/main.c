#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>


int main(){

	printf("starting parent\n");
	int fd_pipe[2];

	pipe(fd_pipe);

	pid_t pid = fork();
	if(!pid){
		//child
		dup2(fd_pipe[1], 1);
		close(fd_pipe[0]);
		close(fd_pipe[1]);
		char name[5] = "ls";
		char *argv[2];
		argv[0] = name;
		argv[1] = 0;
		execv("/bin/ls", argv);
	}

	close(fd_pipe[1]);

	char buffer[100];
	int n = read(fd_pipe[0], buffer, 99);
	buffer[n] = 0;
	printf("output from pipe is: %s\n", buffer);

	printf("ending parent\n");

	return 0;

}//main*/